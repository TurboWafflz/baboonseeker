import java.util.*;

public class baboonSeeker {
    public static void main(String[] args) {
        System.out.println("WELCOME TO BABOON SEEKER XTREME JAVA");
        List<String> inventory = new ArrayList<String>();
        inventory.add("Baboon Seeking Backpack");
        String seekedThing = null;
        String command;
        int bamboo = 5 + (int) (Math.random() * ((20 - 5) + 1));
        System.out.println(
                "You are the mighty baboon seeker. You have set off into the tropics with your baboon seeking backpack holding "
                        + bamboo + " bamboo to use for seeking baboons.");
        int streak = 0;
        while (true) {
            if (!inventory.contains("Baboon Seeking Backpack")) {
                fail();
            }

            System.out.print(": ");
            Scanner scanner = new Scanner(System.in);
            try {
                command = scanner.nextLine();
            } catch (NoSuchElementException | IndexOutOfBoundsException e) {
                command = "";
            }
            if (command.equals("seek")) {
                System.out.println("Seeking...");
                int seeked = (int) (Math.random() * ((100) + 1));
                if (bamboo > 0) {
                    bamboo--;
                } else {
                    System.out.println(
                            "You do not have a bamboo. You must seek one before you will be able to seek baboons.");
                }
                if (seeked == 0) {
                    streak = 0;
                    System.out.println("You soke a Bamboon");
                    seekedThing = "Bamboon";
                    List<Object> stumbled = stumble(inventory, bamboo);
                    inventory = (List) stumbled.get(0);
                    bamboo = (int) stumbled.get(1);
                } else if (seeked < 11 && seeked > 0) {
                    if (bamboo > 0) {
                        streak++;
                        seekedThing = "Baboon";
                    } else {
                        System.out.println("You soke a baboon but it ran away.");
                    }
                } else if (seeked < 31 && seeked > 11) {
                    streak = 0;
                    int bamboos = 2 + (int) (Math.random() * ((12 - 2) + 1));
                    seekedThing = bamboos + " Bamboo";
                    bamboo = bamboo + bamboos;
                } else {
                    streak = 0;
                    seekedThing = "nothing";
                }
                System.out.println("You soke a " + seekedThing + "!");
                if (streak > 1) {
                    System.out.println("STREAK: " + streak);
                }
                assert seekedThing != null;
                if (!seekedThing.equals("nothing") && !seekedThing.contains("Bamboo")) {
                    inventory.add(seekedThing);
                }
            } else if (command.equals("WHAT DOES I HAVE")) {
                Collections.sort(inventory);
                for (String item : inventory) {
                    if (item.equals("Baboon Seeking Backpack")) {
                        System.out.println("YOU HAS " + bamboo + " BAMBOO IN YOUR BABOON SEEKING BACKPACK");
                    } else {
                        System.out.println("YOU HAS A " + item);
                    }
                }
            } else {
                streak = 0;
                List<Object> stumbled = stumble(inventory, bamboo);
                inventory = (List) stumbled.get(0);
                bamboo = (int) stumbled.get(1);
            }
        }
    }

    public static void fail() {
        int points = 0;
        while (true) {
            System.out.print("Failure");
            for (int i = 0; i < points; i++) {
                System.out.print("!");
            }
            System.out.println();
            points++;
        }
    }

    public static List<Object> stumble(List inventory, int bamboo) {
        int droppedItem = (int) (Math.random() * ((inventory.size() - 1) + 1));
        if (droppedItem < inventory.size()) {
            System.out.println("You stumbled and dropped a " + inventory.get(droppedItem));
            inventory.remove(droppedItem);
        } else {
            int bamboos = 1 + (int) (Math.random() * ((bamboo - 1) + 1));
            System.out.println("You stumbled and dropped " + bamboos + " bamboo");
            inventory.remove(droppedItem);
            bamboo = bamboo - bamboos;
        }
        return Arrays.asList(inventory, bamboo);
    }

}
