sub randInRange {
	return @_[0] + int(rand((@_[1] + 1) - @_[0]));
}

sub fail {
	for (my $i = 1; $i > 0; $i++) {
		print "Failure";
		for(my $j = 0; $j < $i; $j++){
			print "!";
		}
		print "\n";
	}
}

sub stumble {
	my $droppedType = int(rand(2));

	if ($droppedType == 1 or $bamboo == 0) {
		my $droppedIndex = int(rand(scalar @inventory));
		my $droppedItem = @inventory[$droppedIndex];
		splice @inventory, $droppedIndex, 1;
		printf 'You stumbled and dropped a %s', $droppedItem;
		print "\n";
		if ($droppedItem eq "Baboon Seeking Backpack") {
			fail();
		}
	} else {
		my $bamboos = randInRange(1, $bamboo);
		printf 'You stumbled and dropped %d bamboo', $bamboos;
		print "\n";
		$bamboo -= $bamboos;
	}

}

sub showInv {
	printf 'YOU HAS %d BAMBOO', $bamboo;
	foreach (@inventory) {
		printf ' AND YOU HAS A %s', $_;
	}
	print "\n";
}

sub baboonSeek {
	if ($bamboo > 0) {
		$bamboo--;
	} else {
		print "YOU HAS NO BAMBOO!\n";
	}

	my $seeked = randInRange(0, 100);
	if ($seeked < 1) {
		print "You soke a Bamboon\n";
		stumble();
	} elsif ($seeked < 10) {
		if ($bamboo > 0) {
			print "You soke a Baboon\n";
			push(@inventory, "Baboon");
		} else {
			print "You soke a Baboon but it ran away because you are out of bamboo\n";
		}
	} elsif ($seeked < 50) {
		print "You soke a Bamboo\n";
		$bamboo+=2;
	} else {
		print "You soke a nothing\n";
	}
}

sub main {
	print "WELCOME TO BABOON SEEKER XTREME PERL\n";
	$bamboo = randInRange(5, 20);
	@inventory = ("Baboon Seeking Backpack");
	printf 'You are the mighty baboon seeker. You have set off into the temple with your baboon seeking backpack holding %d bamboo to use for seeking baboons.', $bamboo;
	print "\n";

	while (42) {
		print ": ";
		my $cmd = <>;
		chomp($cmd);

		if ($cmd eq "seek") {
			baboonSeek();
		} elsif ($cmd eq "WHAT DOES I HAS") {
			showInv();
		} else {
			stumble();
		}

	}
}

main();