extern crate pancurses;

use pancurses::{
    endwin, init_pair, initscr, newwin, noecho, start_color, Input, A_BOLD, COLOR_BLACK,
    COLOR_CYAN, COLOR_GREEN, COLOR_MAGENTA, COLOR_PAIR, COLOR_RED,
};
use rand::Rng;
use std::sync::Mutex;

static BAMBOO: Mutex<u64> = Mutex::new(0);
static BABOONS: Mutex<u64> = Mutex::new(0);
static BACKPACK: Mutex<bool> = Mutex::new(true);

fn stumble(reason: &str) -> String {
    // Lock the variables we need to calculate the drop chances
    let bamboo_guard = BAMBOO.lock().unwrap();
    let baboon_guard = BABOONS.lock().unwrap();
    let backpack_guard = BACKPACK.lock().unwrap();

    // Calculate drop chances
    let total = *bamboo_guard + *baboon_guard + *backpack_guard as u64;
    drop(backpack_guard);
    let bamboo_chance = (*bamboo_guard as f64 / total as f64 * 100.) as u8;
    drop(bamboo_guard);
    let baboon_chance = (*baboon_guard as f64 / total as f64 * 100.) as u8;
    drop(baboon_guard);

    // Select an item to drop
    let selector = rand::thread_rng().gen_range(0..99);
    let dropped_item = if selector <= bamboo_chance {
        let mut bamboo_guard = BAMBOO.lock().unwrap();
        *bamboo_guard -= 1;
        drop(bamboo_guard);
        "bamboo"
    } else if selector <= bamboo_chance + baboon_chance {
        let mut baboon_guard = BABOONS.lock().unwrap();
        *baboon_guard -= 1;
        drop(baboon_guard);
        "baboon"
    } else {
        let mut backpack_guard = BACKPACK.lock().unwrap();
        *backpack_guard = false;
        drop(backpack_guard);
        "backpack"
    };

    // Return text for what happened
    if reason == "Bamboon" {
        "You soke a Bamboon! He exploded causing you to sumble and drop a ".to_string()
            + dropped_item
    } else {
        "You stumbled and dropped a ".to_string() + dropped_item
    }
}

fn game_logic(command: String) -> String {
    // Seek
    if command == "seek" {
        // Select what to seek
        let selector = rand::thread_rng().gen_range(0..99);
        if selector == 0 {
            let mut bamboo_guard = BAMBOO.lock().unwrap();
            if *bamboo_guard > 0 {
                *bamboo_guard -= 1;
                drop(bamboo_guard);
                return stumble("Bamboon");
            }
            drop(bamboo_guard);
            return "You soke a bamboon! Luckily, since YOU HAS NO BAMBOO, it left you alone"
                .to_string();
        } else if selector <= 10 {
            let mut bamboo_guard = BAMBOO.lock().unwrap();
            if *bamboo_guard == 0 {
                drop(bamboo_guard);
                return "You soke a baboon but it ran away because YOU HAS NO BAMBOO!".to_string();
            }
            *bamboo_guard -= 1;
            drop(bamboo_guard);
            let mut baboon_guard = BABOONS.lock().unwrap();
            *baboon_guard += 1;
            drop(baboon_guard);
            return "You soke a baboon!".to_string();
        } else if selector <= 60 {
            let mut bamboo_guard = BAMBOO.lock().unwrap();
            *bamboo_guard += 1;
            drop(bamboo_guard);
            return "You soke a bamboo!".to_string();
        } else {
            let mut bamboo_guard = BAMBOO.lock().unwrap();
            if *bamboo_guard > 0 {
                *bamboo_guard -= 1;
            }
            drop(bamboo_guard);
            return "You soke a nothing".to_string();
        }
    }

    stumble("invalid")
}

fn main() {
    // Give player initial bamboo
    let mut bamboo_guard = BAMBOO.lock().unwrap();
    *bamboo_guard = rand::thread_rng().gen_range(5..20);
    drop(bamboo_guard);

    let screen = initscr();
    if screen.get_max_x() < 59 || screen.get_max_y() < 19 {
        endwin();
        println!("Your terminal is not big enough to seek baboons");
        return;
    }

    let mut command_window = newwin(0, 0, 0, 0);
    let mut history_window = newwin(0, 0, 0, 0);
    let mut status_window = newwin(0, 0, 0, 0);

    noecho();

    start_color();
    // Setup palettes
    init_pair(1, COLOR_GREEN, COLOR_BLACK);
    init_pair(2, COLOR_RED, COLOR_BLACK);
    init_pair(3, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(4, COLOR_CYAN, COLOR_BLACK);

    let terminal_y_size = screen.get_max_y();
    let terminal_x_size = screen.get_max_x();

    // Position windows

    // Command window
    command_window.resize(3, terminal_x_size);
    command_window.mvwin(terminal_y_size - 3, 0);

    // History window
    history_window.resize(terminal_y_size - 6, terminal_x_size);
    history_window.mvwin(0, 0);
    // Enable scrolling when the history window fills up
    history_window.scrollok(true);

    // Status window
    status_window.resize(3, terminal_x_size);
    status_window.mvwin(terminal_y_size - 6, 0);
    status_window.draw_box(0, '#' as u32);

    // Refresh so our windows actually exist
    screen.refresh();

    // Label history window
    history_window.attron(A_BOLD);
    history_window.attron(COLOR_PAIR(4));
    history_window.mvprintw(0, 0, "History:\n\n");
    history_window.attroff(COLOR_PAIR(4));
    history_window.attroff(A_BOLD);

    // Label status window
    status_window.attron(A_BOLD);
    status_window.mvprintw(0, 1, "Status:");
    status_window.attroff(A_BOLD);

    // Initial message
    let bamboo_guard = BAMBOO.lock().unwrap();
    history_window.printw("WELCOME TO BABOON SEEKER XTREME RUST CURSES!\n");
    history_window.printw(format!("You are a mighty baboon seeker who has set off into the rusted sector of the linguistic plane with {:?} bamboo to use for seeking baboons\n", *bamboo_guard));
    drop(bamboo_guard);

    // Status window text
    status_window.attron(A_BOLD);
    status_window.attron(COLOR_PAIR(1));
    status_window.mvprintw(1, 1, "Bamboo:");
    status_window.attron(COLOR_PAIR(2));
    status_window.mvprintw(1, 21, "Baboons:");
    status_window.attron(COLOR_PAIR(3));
    status_window.mvprintw(1, 41, "Backpack:");
    status_window.attroff(COLOR_PAIR(3));
    status_window.attroff(A_BOLD);

    history_window.refresh();
    status_window.refresh();

    loop {
        // Update status
        let bamboo_guard = BAMBOO.lock().unwrap();
        let baboon_guard = BABOONS.lock().unwrap();
        let backpack_guard = BACKPACK.lock().unwrap();
        status_window.mvprintw(1, 9, format!("{}    ", *bamboo_guard));
        status_window.mvprintw(1, 30, format!("{}    ", *baboon_guard));
        status_window.mvprintw(1, 51, format!("{}    ", *backpack_guard as u8));
        status_window.refresh();
        drop(bamboo_guard);
        drop(baboon_guard);
        // The backpack guard will get dropped later

        // Display prompt in command window
        command_window.mvprintw(1, 1, "Command: ");
        command_window.clrtoeol();
        command_window.draw_box(0, 0);

        // Make sure the user has a backpack
        if !*backpack_guard {
            history_window.printw("OH NO! YOU HAVE DROPPED YOUR BABOON SEEKING BACKPACK AND NOW MUST RETIRE FROM SEEKING FOREVER");
            history_window.refresh();
            history_window.getch();
            break;
        }
        drop(backpack_guard);

        // Read in a command
        let mut reading_command = true;
        let mut command = String::new();
        while reading_command {
            let input = match command_window.getch() {
                Some(Input::Character(c)) => c,
                _ => 0 as char,
            };

            match input {
                '\n' => {
                    reading_command = false;
                }

                // Backspace
                _ if (input == 127 as char || input == 8 as char) && !command.is_empty() => {
                    // Get cursor position
                    let y = command_window.get_cur_y();
                    let x = command_window.get_cur_x();

                    // Move cursor back
                    command_window.mv(y, x - 1);
                    // Delete this character
                    command_window.delch();
                    // Remove this character from the command buffer
                    command.remove(command.len() - 1);
                }

                _ if (input >= 20 as char
                    && input <= 126 as char
                    && command.len() < (terminal_x_size - 11) as usize) =>
                {
                    command_window.printw(input.to_string());
                    command += &input.to_string();
                }

                _ => {}
            }
        }

        // Print command to history
        history_window.printw(command.clone() + ":\n");

        // Do game logic
        history_window.printw(game_logic(command) + "\n");

        history_window.refresh();
    }

    endwin();
}
