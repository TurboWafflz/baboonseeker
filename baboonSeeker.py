import random
import colorama
import time
import threading
import itertools
import sys

global done
done = False

def shell():
	while True:
		try:
			output = eval(input(">>> "))
		except KeyboardInterrupt:
			return
		except Exception as e:
			output = e
		if output != None:
			print(output)

def seeking():
	for c in itertools.cycle(["|", "/", "-", "\\"]):
		if done:
			break
		sys.stdout.write("\rSeeking... " + c)
		sys.stdout.flush()
		time.sleep(0.1)
	sys.stdout.write("\n")

def fail():
	points=0
	while True:
		print("Failure", end='')
		for i in range(points):
			print("!", end='')
		print("\n", end='')
		points += 1

def stumble(inventory,bamboo):
	droppedItem = random.randint(0,len(inventory))
	if droppedItem < len(inventory):
		print("You stumbled and dropped a {}".format(inventory[droppedItem]))
		input("[Press enter to continue]")
		inventory.pop(droppedItem)
	else:
		bamboos = random.randint(1,bamboo)
		print("You stumbled and dropped {} bamboo".format(bamboos))
		input("[Press enter to continue]")
		bamboo -= bamboos
	return inventory,bamboo

def main():
	print("WELCOME TO BABOON SEEKER XTREME PYTHON")
	inventory = ["Baboon Seeking Backpack"]
	bamboo = random.randint(5,20)
	print("You are the mighty baboon seeker. You have set off into the forest with your baboon seeking backpack holding {} bamboo to use for seeking baboons.".format(str(bamboo)))
	streak = 0
	while True:
		if not "Baboon Seeking Backpack" in inventory:
			fail()
		command = input(": ")
		if command == "seek":
			print("Seeking...")
			# t = threading.Thread(target=seeking)
			# t.start()
			#time.sleep(random.randint(0,10))
			done = True
			seeked = random.randint(0,100)
			#print(seeked)
			if bamboo > 0:
				bamboo -= 1
			else:
				print("You do not have a bamboo. You must seek one before you will be able to seek baboons.")
			if seeked == 0:
				streak = 0
				print("You soke a Bamboon")
				seekedThing = "Bamboon"
				inventory,bamboo=stumble(inventory,bamboo)
			elif seeked == 1:
				streak = 0
				seekedThing = "Python"
				print("You soke a Python")
				shell()
			elif seeked < 11 and seeked > 0:
				if bamboo > 0:
					streak += 1
					seekedThing = "Baboon"
				else:
					print("You soke a baboon but it ran away.")
			elif seeked < 31 and seeked > 11:
				streak = 0
				bamboos = random.randint(2,12)
				seekedThing = "{} Bamboo".format(str(bamboos))
				bamboo += bamboos
			else:
				streak = 0
				seekedThing = "nothing"
			if seekedThing != "Python":
				print("You soke a {}!".format(seekedThing))
			if streak > 1:
				print("STREAK: {}".format(str(streak)))
			if seekedThing != "nothing" and "Bamboo" not in seekedThing and seekedThing != "Python":
				inventory.append(seekedThing)
		elif command == "WHAT DOES I HAVE":
			for item in sorted(inventory):
				if item == "Baboon Seeking Backpack":
					print("YOU HAS {} BAMBOO IN YOUR BABOON SEEKING BACKPACK".format(str(bamboo)))
				else:
					print("YOU HAS A {}".format(item))
		else:
			inventory,bamboo = stumble(inventory,bamboo)

if __name__ == "__main__":
	main()