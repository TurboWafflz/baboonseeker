fail <- function() {
    i <- 1
    while (i > 0) {
        cat("Failure")
        for (j in 0:i) {
            cat("!")
        }
        cat("\n")
        i <- i + 1
    }
}

stumble <- function(inventory, bamboo) {
    droppedType <- sample(1:2, 1)
    if (bamboo == 0) {
        droppedType <- 1
    }

    if (droppedType == 1) {
        droppedIndex <- sample(length(inventory), 1)

        droppedItem <- inventory[droppedIndex]
        inventory <- inventory[-droppedIndex]

        cat("You stumbled and dropped a", droppedItem, "\n")
        if (droppedItem == "Baboon Seeking Backpack") {
            fail()
        }
        return (list(inventory = inventory, bamboo = bamboo))
    } else {
        bamboos <- sample(1:bamboo, 1)
        cat("You stumbled and dropped", bamboos, "bamboo\n")
        bamboo <- bamboo - bamboos
        return (list(inventory = inventory, bamboo = bamboo))
    }
}

showInv <- function(inventory, bamboo) {
    cat("YOU HAS", bamboo, "BAMBOO")
    for (value in inventory) {
        cat(" AND YOU HAS A", value)
    }
    cat("\n")
}

seek <- function(inventory, bamboo) {
    if (bamboo > 0) {
        bamboo <- bamboo - 1
    } else {
        cat("YOU HAS NO BAMBOO!\n")
    }

    seeked <- sample(0:100, 1)
    if (seeked < 1) {
        cat("You soke a Bamboon\n")
        newVals <- stumble(inventory, bamboo)
        inventory <- newVals$inventory
        bamboo <- newVals$bamboo
    } else if (seeked < 10) {
        if (bamboo > 0) {
            cat("You soke a Baboon\n")
            inventory <- append(inventory, "Baboon")
        } else {
            cat("You soke a Baboon but it ran away because you are out of bamboo\n")
        }
    } else if (seeked < 50) {
        cat("You soke a Bamboo\n")
        bamboo <- bamboo + 2
    } else {
        cat("You soke a nothing\n")
    }

    return (list(inventory = inventory, bamboo = bamboo))
}

main <- function() {
    cat("WELCOME TO BABOON SEEKER XTREME R\n")
    bamboo <- sample(5:20, 1)
    inventory <- c("Baboon Seeking Backpack")
    
    cat("You are the mighty baboon seeker. You have set off into the tunnel with your baboon seeking backpack holding", bamboo, "bamboo to use for seeking baboons.\n")

    while (TRUE) {
        cat(": ")
        cmd <- readLines("stdin", n=1)
        if (cmd == "seek") {
            newVals <- seek(inventory, bamboo)
            inventory <- newVals$inventory
            bamboo <- newVals$bamboo
        } else if (cmd == "WHAT DOES I HAS") {
            showInv(inventory, bamboo)
        } else {
            newVals <- stumble(inventory, bamboo)
            inventory <- newVals$inventory
            bamboo <- newVals$bamboo
        }
    }
}

main()
