#!/bin/bash
cargo build --target x86_64-unknown-uefi
cp target/x86_64-unknown-uefi/debug/baboon_seeker_uefi.efi esp/efi/boot/bootx64.efi
qemu-system-x86_64 -enable-kvm \
                   -drive if=pflash,format=raw,readonly=on,file=OVMF_CODE.4m.fd \
                   -drive if=pflash,format=raw,readonly=on,file=OVMF_VARS.4m.fd \
                   -drive format=raw,file=fat:rw:esp

