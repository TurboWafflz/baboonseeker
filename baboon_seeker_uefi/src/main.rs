#![no_main]
#![no_std]

use core::fmt::Write;

use uefi::{prelude::*, print, proto::console::text::{Input, Output, ScanCode}, Char16};


fn input() -> [char; 255]{
    let output_handle = boot::get_handle_for_protocol::<Output>().unwrap();
    let mut output = boot::open_protocol_exclusive::<Output>(output_handle).unwrap();
    
    let input_handle = boot::get_handle_for_protocol::<Input>().unwrap();
    let mut input = boot::open_protocol_exclusive::<Input>(input_handle).unwrap();
    
    let mut text = ['\0'; 255];
    let mut index = 0;
    let mut character = Char16::default();
    while character != Char16::try_from('\n' as u16).unwrap() && index < 255{
        let key = input.read_key().unwrap();
        character = match key{
            Some(uefi::proto::console::text::Key::Printable(c)) => c,
            _ => Char16::default()
        };
        if character != Char16::default() && index < 255{
            output.write_char(character.into());
            text[index] = character.into();
            index += 1;
        }
    }

    return text;
}

#[entry]
fn main() -> Status {
    uefi::helpers::init().unwrap();
    let output_handle = boot::get_handle_for_protocol::<Output>().unwrap();
    let mut output = boot::open_protocol_exclusive::<Output>(output_handle).unwrap();


    output.clear();
    output.write_str("WELCOME TO BABOONSEEKER XTREME UEFI\n: ");
    drop(output);

    input();
    Status::SUCCESS
}
