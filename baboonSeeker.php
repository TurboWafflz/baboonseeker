<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Baboon Seeker PHP</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
	<?php
	function main(){
		$html = "WELCOME TO BABOON SEEKER XTREME PHP<br>";
		$bamboo = mt_rand(5, 20);
		$inventory = array("Baboon Seeking Backpack");
		$_SESSION['bamboo'] = $bamboo;
		$_SESSION['inventory'] = $inventory;
		$html = $html . "You are the mighty baboon seeker. You have set off into the savanna with your baboon seeking backpack holding " . $bamboo . " bamboo to use for seeking baboons.<br><br>";
		$html = $html . "<div class=\"response\"></div><br><br><input id=\"inputBox\" type=\"text\"></input><div id=\"bottom\"></div>";
		return $html;
	}

	function seek(){
		$response_prefix = "";
		$bamboo = $_SESSION['bamboo'];
	
		if ($bamboo > 0) {
			$bamboo--;
			$_SESSION['bamboo'] = $bamboo;
		} else {
			$response_prefix = "YOU HAS NO BAMBOO!<br>";
		}

		$seeked = mt_rand(0, 100);
		if($seeked < 1){
			return $response_prefix . "You soke a bamboon";
		}else if($seeked < 10){
			if ($bamboo > 0) {
				$inventory = $_SESSION['inventory'];
				array_push($inventory, "Baboon");
				$_SESSION['inventory'] = $inventory;
				return $response_prefix . "You soke a Baboon";
			} else {
				return $response_prefix . "You soke a Baboon but it ran away because you are out of bamboo";
			}
		}else if($seeked < 50){
			$bamboo+=2;
			$_SESSION['bamboo'] = $bamboo;
			return $response_prefix . "You soke a Bamboo";
		}else{
			return $response_prefix . "You soke a nothing";
		}
	}

	function stumble(){
		$droppedType = mt_rand(1, 2);
		$bamboo = $_SESSION['bamboo'];
		$inventory = $_SESSION['inventory'];
		if($bamboo == 0){
			$droppedType = 1;
		}
		if ($droppedType == 1){
			$droppedItem = mt_rand(0, count($inventory) - 1);
			$droppedItemName = $inventory[$droppedItem];
			unset($inventory[$droppedItem]);
			$_SESSION['inventory'] = $inventory;
			return "You stumbled and dropped a " . $droppedItemName;
		}else{
			$bamboos = mt_rand(1, $bamboo);
			$bamboo -= $bamboos;
			$_SESSION['bamboo'] = $bamboo;
			return "You stumbled and dropped " . $bamboos . " bamboo";
		}
	}

	function showInv(){
		$bamboo = $_SESSION['bamboo'];
		$inventory = $_SESSION['inventory'];
		$html = "YOU HAS " . $bamboo . " BAMBOO";
		foreach($inventory as &$item){
			$html = $html . " AND YOU HAS A " . $item;
		}
		return $html;
	}

	function fail(){
		return "<script>function sleep(ms) {return new Promise(resolve => setTimeout(resolve, ms));}async function fail(){for (var i = 1; i > 0; i++){var text = \"Failure\";for(var j = 0; j < i; j++){text = text + \"!\";}text = text + \"<br>\";await sleep(1);$(\".response\").append(text);document.getElementById(\"bottom\").scrollIntoView();}} fail();</script>";
	}

	if(!isset($_GET['command'])){
		echo main();
	}

	if(isset($_POST['cmd'])){
		if ($_POST['cmd'] == 'seek'){
			$resp = seek();
			$resp = $resp . "<br>";
			if($resp == "You soke a Bamboon<br>"){
				$new_resp = stumble();
				$new_resp = $new_resp . "<br>";
				if($new_resp == "You stumbled and dropped a Baboon Seeking Backpack<br>"){
					$resp = $resp . $new_resp;
					$resp = $resp . fail();
				}
			}
			echo $resp;
		}else if($_POST['cmd'] == 'WHAT DOES I HAS'){
			echo showInv() . "<br>";
		}else{
			$resp = stumble();
			$resp = $resp . "<br>";
			if($resp == "You stumbled and dropped a Baboon Seeking Backpack<br>"){
				$resp = $resp . fail();
			}
			echo $resp;
		}
	}
	?>
	<script>

	$('#inputBox').keypress(function (e) {
		if (e.which == 13 && $('#inputBox').val() != "") {
			$.ajax({
				url: <?php echo '\'' . $_SERVER['PHP_SELF'] . '?command=true\','; ?>

				type: 'post',
				data: {'cmd': $('#inputBox').val()},
				async: false,
				success: function(data) {
					$('#inputBox').val("");
					$(".response").append(data);
					document.getElementById("bottom").scrollIntoView();
				}
			});
			return false;
		}
	});

</script>
</body>
</html>
