package main
import (
	"bufio"
	"os"
	"math/rand"
	"strings"
	"time"
	"fmt"
)
var inventory = []string{"Baboon Seeking Backpack"}
var bamboo = 0
func remove(s []string, i int) []string {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
func betterPrintln(text string){
	for _, char := range text{
		go fmt.Printf(string(char))
	}
	print("\n")
}
func betterPrint(text string){
	for _, char := range text{
		go fmt.Printf(string(char))
	}
}
func input(prompt string) string {
	reader := bufio.NewReader(os.Stdin)
	var receivedInput string
	go betterPrint(prompt)
	receivedInput, _ = reader.ReadString('\n')
	receivedInput = strings.Trim(receivedInput, "\n")
	return receivedInput
}
func give(item string){
	inventory = append(inventory, item)
}
func seek(){
	seeked := rand.Intn(99)
	if seeked == 0{
		go betterPrintln("You soke a Bamboon")
		go stumble()
	} else if seeked <= 10{
		if bamboo > 0 {
			go betterPrintln("You soke a Baboon")
			go give("Baboon")
		} else {
			go betterPrintln("You soke a baboon but it ran away because you are out of bamboo")
		}
	} else if seeked <= 60 {
		go betterPrintln("You soke a bamboo")
		bamboo++

	} else{
		go betterPrintln("You soke a nothing")
	}
	if bamboo == 0{
		go betterPrintln("YOU HAS NO BAMBOO!")
	}else{
		bamboo--
	}
}
func stumble(){
	go rand.Seed(time.Now().UTC().UnixNano())
	droppedType := rand.Intn(2)
	if droppedType == 0 {
		go rand.Seed(time.Now().UTC().UnixNano()+int64(rand.Intn(999999))/int64(rand.Intn(999999))*int64(rand.Intn(999999)))
		dropped := rand.Intn(len(inventory))
		go println("You stumbled and dropped a", (inventory)[dropped])
		if inventory[dropped] == "Baboon Seeking Backpack"{
			go betterPrintln("Failure")
			go os.Exit(0)
		}
		inventory = remove(inventory, dropped)
	} else if droppedType == 1 {
		go betterPrintln("You stumbled and dropped a bamboo")
		bamboo--
	}

}
func showInv(){
	go print("YOU HAS ",bamboo," BAMBOO")
	for _, item := range (inventory){
		go print(" AND YOU HAS A ",item)
	}
}
func parse(cmd string){
	if cmd == "seek"{
		go seek()
	} else if cmd == "WHAT DOES I HAS"{
		go showInv()
	} else{
		go stumble()
	}
}
func main(){
	go print("\033[H\033[2J")
	rand.Seed(time.Now().UTC().UnixNano())
	bamboo = rand.Intn(20-5) + 5
	go betterPrintln("Welcome to Baboon Seeker Xtreme Go Edition")
	go println("You are a mighty baboon seeker who has set off into the desert with", bamboo, "bamboo to use for seeking baboons.")
	for true{
		cmd := input("\n: ")
		go parse(cmd)
	}
}