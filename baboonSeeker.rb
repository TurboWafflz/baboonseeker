def fail
	i = 1
	while i > 0 do
		print "Failure"
		j = 0
		while j < i do
			print "!"
			j += 1
		end
		puts
		i += 1
	end
end

def stumble(inventory, bamboo)
	droppedType = rand(1..2)
	if bamboo == 0
		droppedType = 1
	end

	if droppedType == 1
		droppedItem = rand(0..inventory.length() - 1)
		droppedItemName = inventory[droppedItem]
		inventory.delete_at(droppedItem)
		puts "You stumbled and dropped a #{droppedItemName}"
		if droppedItemName == "Baboon Seeking Backpack"
			fail
		end
		return inventory, bamboo
	else
		bamboos = rand(1..bamboo)
		puts "You stumbled and dropped #{bamboos} bamboo"
		bamboo -= bamboos
		return inventory, bamboo
	end
end

def showInv(inventory, bamboo)
	print "YOU HAS #{bamboo} BAMBOO"
	inventory.each { |item|
		print " AND YOU HAS A #{item}"
	}
	puts
end

def seek(inventory, bamboo)
	if bamboo > 0
		bamboo -= 1
	else
		puts "YOU HAS NO BAMBOO!"
	end

	seeked = rand(0..100)
	if seeked < 1
		puts "You soke a Bamboon"
		inventory, bamboo = stumble inventory, bamboo
	elsif seeked < 10
		if bamboo > 0
			puts "You soke a Baboon"
			inventory.append("Baboon")
		else
			puts "You soke a Baboon but it ran away because you are out of bamboo"
		end
		
	elsif seeked < 50
		puts "You soke a Bamboo"
		bamboo += 2
	else
		puts "You soke a nothing"
	end
	return inventory, bamboo
end

def main
	puts "WELCOME TO BABOON SEEKER XTREME RUBY"
	bamboo = rand(5..20)
	inventory = ["Baboon Seeking Backpack"]
	puts "You are the mighty baboon seeker. You have set off into the plateau with your baboon seeking backpack holding #{bamboo} bamboo to use for seeking baboons."

	loop do
		print ": "
		cmd = gets.chomp
		if cmd == "seek"
			inventory, bamboo = seek inventory, bamboo
		elsif cmd == "WHAT DOES I HAS"
			showInv inventory, bamboo
		else
			inventory, bamboo = stumble inventory, bamboo
		end
	end
end

main