#include "baboon.h"
#include <PalmTypes.h>
#include <PalmCompatibility.h>
#include <System/SystemPublic.h>
#include <UI/UIPublic.h>


// Gets the content of a field in a form
static Char* GetField(UInt16 formID, UInt16 fieldID)
{
	FormPtr 			frm;
	FieldPtr			fld;
	UInt16				obj;

	frm = FrmGetFormPtr(formID);
	obj = FrmGetObjectIndex(frm, fieldID);
	fld = (FieldPtr)FrmGetObjectPtr(frm, obj);
	return 	FldGetTextPtr(fld);
}


// Sets the content of a field in a form
static FieldPtr SetField(UInt16 formID, UInt16 fieldID, MemPtr str)
{
	FormPtr 	frm;
	FieldPtr	fld;
	UInt16		obj;
	CharPtr		p;
	VoidHand	h;

	frm = FrmGetFormPtr(formID);
	obj = FrmGetObjectIndex(frm, fieldID);
	fld = (FieldPtr)FrmGetObjectPtr(frm, obj);
	h = (VoidHand)FldGetTextHandle(fld);
	if (h == NULL)
	{
		h = MemHandleNew (FldGetMaxChars(fld)+1);
		ErrFatalDisplayIf(!h, "No Memory");
	}

	p = (CharPtr)MemHandleLock(h);
	StrCopy(p, str);
	MemHandleUnlock(h);

	FldSetTextHandle(fld, (Handle)h);
	FldDrawField(fld);
}

// Main game logic
Char* run(){
	Char* cmd = GetField(Game, CmdLine);
	if(strcmp(cmd, "seek") == 0){
		return("seek");
	} else if(strcmp(cmd, "WHAT DOES I HAS") == 0){
		return("inventory");
	} else{
		return("stumble");
	}
}

UInt32 PilotMain(UInt16 cmd, MemPtr cmdPBP, UInt16 launchFlags)
{
	short err;
	EventType e;
	FormType *pfrm;

	if (cmd == sysAppLaunchCmdNormalLaunch)			// Make sure only react to NormalLaunch, not Reset, Beam, Find, GoTo...
	{
		// Start on game form
		FrmGotoForm(Game);

		while(1)
		{
			// No clue what this does
			EvtGetEvent(&e, 100);
			if (SysHandleEvent(&e))
				continue;
			if (MenuHandleEvent((void *)0, &e, &err))
				continue;

			// Handle enter, this should probably be done in the switch case below, but for some reason if I do that then I can't type
			if(e.eType == keyDownEvent && e.data.keyDown.chr == chrLineFeed){
				SetField(Game, Output, run());
				SetField(Game, CmdLine, "");
			}
			switch (e.eType)
			{
					// React to controls
					case ctlSelectEvent:
						switch (e.data.ctlSelect.controlID){
							case Go:
								SetField(Game, Output, run());
								SetField(Game, CmdLine, "");
								continue;
							default:
								continue;
						}
						goto _default;
						break;

					// No clue what this does
					case frmLoadEvent:
						FrmSetActiveForm(FrmInitForm(e.data.frmLoad.formID));
						break;
					// This probably makes the form actually render, but I don't really know
					case frmOpenEvent:
						pfrm = FrmGetActiveForm();
						FrmDrawForm(pfrm);
						break;

					// React to menu events
					case menuEvent:
						switch (e.data.menu.itemID){
							default:
								continue;
						}

					case appStopEvent:
						goto _quit;

					default:
_default:
						if (FrmGetActiveForm())
							FrmHandleEvent(FrmGetActiveForm(), &e);
			}
		}

_quit:
		FrmCloseAllForms();

	}

	return 0;
}