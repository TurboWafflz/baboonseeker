import random
import sys

# this version is written for Python 1.6.1

def fail():
	i = 1
	while i > 0:
		sys.stdout.write("Failure")
		for j in range(i):
			sys.stdout.write("!")
		print
		i = i + 1

def stumble(inventory, bamboo):
	droppedType = random.randint(0, 2)
	if bamboo == 0:
		droppedType = 1

	if droppedType == 1:
		droppedItem = inventory.pop(random.randint(0, len(inventory) - 1))
		print "You stumbled and dropped a %s" % droppedItem
		if droppedItem == "Baboon Seeking Backpack":
			fail()

		return inventory, bamboo
	else:
		bamboos = random.randint(1, bamboo)
		print "You stumbled and dropped %s bamboo" % bamboos
		bamboo = bamboo - bamboos
		return inventory, bamboo

def showInv(inventory, bamboo):
	sys.stdout.write("YOU HAS %s BAMBOO" % bamboo)
	for item in inventory:
		sys.stdout.write(" AND YOU HAS A %s" % item)
	print

def seek(inventory, bamboo):
	if bamboo > 0:
		bamboo = bamboo - 1
	else:
		print "YOU HAS NO BAMBOO!"

	seeked = random.randint(0, 100)
	if seeked < 1:
		print "You soke a Bamboon"
		inventory, bamboo = stumble(inventory, bamboo)
	elif seeked < 10:
		if bamboo > 0:
			print "You soke a Baboon"
			inventory.append("Baboon")
		else:
			print "You soke a Baboon but it ran away because you are out of bamboo"
	elif seeked < 50:
		print "You soke a Bamboo"
		bamboo = bamboo + 2
	else:
		print "You soke a nothing"
	return inventory, bamboo

def main():
	print "WELCOME TO BABOON SEEKER XTREME PYTHON 1"
	bamboo = random.randint(5, 20)
	inventory = ["Baboon Seeking Backpack"]
	print "You are the mighty baboon seeker. You have set off into the mistake with your baboon seeking backpack holding %s bamboo to use for seeking baboons." % bamboo

	while 1:
		cmd = raw_input(": ")

		if cmd == "seek":
			inventory, bamboo = seek(inventory, bamboo)
		elif cmd == "WHAT DOES I HAS":
			showInv(inventory, bamboo)
		else:
			inventory, bamboo = stumble(inventory, bamboo)

main()
