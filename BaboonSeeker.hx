import Sys;
import Random;

class BaboonSeeker {

	static private function fail():Void {
		var i = 1;
		while (i > 0) {
			Sys.println("Failure");
			var j = 0;
			while (j < i) {
				Sys.print("!");
				j += 1;
			}
			Sys.println("");
			i += 1;
		}
	}

	static private function stumble(inventory:Array<String>, bamboo:Int):Array<Dynamic> {
		var droppedType = Random.int(1, 2);
		if (bamboo == 0) {
			droppedType = 1;
		}

		if (droppedType == 1) {
			var droppedItem = Random.int(0, inventory.length - 1);
			var droppedItemName = inventory[droppedItem];
			inventory.remove(droppedItemName);
			Sys.println('You stumbled and dropped a $droppedItemName');
			if (droppedItemName == "Baboon Seeking Backpack") {
				fail();
			}
			return [inventory, bamboo];
		} else {
			var bamboos = Random.int(1, bamboo);
			Sys.println('You stumbled and dropped $bamboos bamboo');
			bamboo -= bamboos;
			return [inventory, bamboo];
		}
	}

	static private function showInv(inventory:Array<String>, bamboo:Int):Void {
		Sys.print('YOU HAS $bamboo BAMBOO');

		for (item in inventory) {
			Sys.print(' AND YOU HAS A $item');
		}

		Sys.println("");
	}

	static private function seek(inventory:Array<String>, bamboo:Int):Array<Dynamic> {
		if (bamboo > 0) {
			bamboo--;
		} else {
			Sys.println("YOU HAS NO BAMBOO!");
		}

		var seeked = Random.int(0, 100);
		if (seeked < 1) {
			Sys.println("You soke a Bamboon");
			var returnValue = stumble(inventory, bamboo);
			inventory = returnValue[0];
			bamboo = returnValue[1];
		} else if (seeked < 10) {
			if (bamboo > 0) {
				Sys.println("You soke a Baboon");
				inventory.push("Baboon");
			} else {
				Sys.println("You soke a Baboon but it ran away because you are out of bamboo");
			}
		} else if (seeked < 50) {
			Sys.println("You soke a Bamboo");
			bamboo += 2;
		} else {
			Sys.println("You soke a nothing");
		}

		return [inventory, bamboo];
	}

	static public function main():Void {
		Sys.println("WELCOME TO BABOON SEEKER XTREME HAXE");
		var bamboo = Random.int(5, 20);
		var inventory = ["Baboon Seeking Backpack"];
		Sys.println('You are the mighty baboon seeker. You have set off into the matrix with your baboon seeking backpack holding $bamboo bamboo to use for seeking baboons.');

		while (true) {
			Sys.print(": ");
			var cmd = Sys.stdin().readLine();
			if (cmd == "seek") {
				var returnValue = seek(inventory, bamboo);
				inventory = returnValue[0];
				bamboo = returnValue[1];
			} else if (cmd == "WHAT DOES I HAS") {
				showInv(inventory, bamboo);
			} else {
				var returnValue = stumble(inventory, bamboo);
				inventory = returnValue[0];
				bamboo = returnValue[1];
			}
		}
	}
}
