function fail()
	local i = 1
	while i > 0 do
		io.write("Failure")
		for j = 0, i do
			io.write("!")
		end
		print()
		i = i + 1
	end
end

function stumble(inventory, bamboo)
	local droppedType = math.random(2)
	if bamboo == 0 then
		droppedType = 1
	end

	if droppedType == 1 then
		droppedItem = table.remove(inventory, math.random(#inventory))
		print("You stumbled and dropped a " .. droppedItem)
		if droppedItem == "Baboon Seeking Backpack" then
			fail()
		end
		return inventory, bamboo
	else
		bamboos = math.random(1, bamboo)
		print("You stumbled and dropped " .. bamboos .. " bamboo")
		bamboo = bamboo - bamboos
		return inventory, bamboo
	end
end

function showInv(inventory, bamboo)
	io.write("YOU HAS " .. bamboo .. " BAMBOO")
	for i, item in ipairs(inventory) do
		io.write(" AND YOU HAS A " .. item)
	end
	print()
end

function seek(inventory, bamboo)
	if bamboo > 0 then
		bamboo = bamboo - 1
	else
		print("YOU HAS NO BAMBOO!")
	end

	seeked = math.random(0, 100)
	if seeked < 1 then
		print("You soke a Bamboon")
		inventory, bamboo = stumble(inventory, bamboo)
	elseif seeked < 10 then
		if bamboo > 0 then
			print("You soke a Baboon")
			table.insert(inventory, "Baboon")
		else
			print("You soke a Baboon but it ran away because you are out of bamboo")
		end
	elseif seeked < 50 then
		print("You soke a Bamboo")
		bamboo = bamboo + 2
	else
		print("You soke a nothing")
	end
	return inventory, bamboo
end

function main()
	print("WELCOME TO BABOON SEEKER XTREME LUA")
	local bamboo = math.random(5, 20)
	local inventory = {"Baboon Seeking Backpack"}
	print("You are the mighty baboon seeker. You have set off into the mountains with your baboon seeking backpack holding " .. bamboo .. " bamboo to use for seeking baboons.")

	while true do
		io.write(": ")
		local cmd = io.read()

		if cmd == "seek" then
			inventory, bamboo = seek(inventory, bamboo)
		elseif cmd == "WHAT DOES I HAS" then
			showInv(inventory, bamboo)
		else
			inventory, bamboo = stumble(inventory, bamboo)
		end
	end
end

main()