#include <ncurses.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

#define MAX_COMMAND_LENGTH 64

static volatile unsigned bamboo = 0;
static volatile unsigned baboons = 0;
static volatile bool backpack = TRUE;

char *stumble(char reason[]){
	char *dropped_item;
	char drop_message[128];
	
	// Calculate item drop chances
	unsigned total = bamboo + baboons + backpack;
	unsigned bamboo_chance = (int) (float) bamboo / (float) total * 100;
	unsigned baboon_chance = (int) (float) baboons / (float) total * 100;

	// Select item to drop
	unsigned selector = rand() % 100;
	if (selector <= bamboo_chance){
		dropped_item = "bamboo";
		bamboo--;
	} else if (selector <= bamboo_chance + baboon_chance){
		dropped_item = "baboon";
		baboons--;
	} else {
		dropped_item = "backpack";
		backpack--;
	}
	
	// Construct message to return
	if (strcmp(reason, "Bamboon") == 0 ){
		sprintf(drop_message, "You soke a Bamboon! He exploded, causing you to stumble and drop a %s", dropped_item);
	} else {
		sprintf(drop_message, "You stumbled and dropped a %s", dropped_item);
	}

	return strdup(drop_message);
}

char *game_logic(char command[]){
	if (strcmp(command, "seek") == 0){
		unsigned selector = rand() % 99;
		if (selector == 0){
			if (bamboo > 0){
				bamboo--;
				return stumble("Bamboon");
			} else {
				return "You soke a bamboon! Luckily, since YOU HAS NO BAMBOO, it left you alone";
			}
		}
		else if (selector <= 10){
			if (bamboo > 0){
				baboons++;
				bamboo--;
			
				return "You soke a baboon!";
			} else {
				return "You soke a baboon but it ran away because YOU HAS NO BAMBOO!";
			}
		}
		else if (selector <= 60) {
			bamboo++;
			return "You soke a bamboo!";
		}
		else {
			if (bamboo > 0){
				bamboo--;
			}
			return "You soke a nothing";
		}
	}

	return stumble("invalid");
}


int main(int argc, char *argv[]){
	// Seed random number generator
	srand(time(NULL));

	// Give player initial bamboo
	bamboo = rand() % (20 - 5) + 5;	

	// Create windows for the different part of the UI
	WINDOW *screen = initscr();
	
	if (getmaxx(screen) < 59 || getmaxy(screen) < 19){
		printf("Your terminal is not big enough to seek baboons");
		return 0;
	}

	WINDOW *command_window = newwin(0, 0, 0, 0);
	WINDOW *history_window = newwin(0, 0, 0, 0);
	WINDOW *status_window = newwin(0, 0, 0, 0); 

	// Disable getch echo
	noecho();

	// Enable color
	start_color();
	
	// Setup palettes
	init_pair(1, COLOR_GREEN, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(4, COLOR_CYAN, COLOR_BLACK);

	bool running = TRUE;
	unsigned terminal_x_size;
	unsigned terminal_y_size;
	
	// Get terminal size
	getmaxyx(screen, terminal_y_size, terminal_x_size);

	// Position our windows

	// Command window
	wresize(command_window, 3, terminal_x_size);
	mvwin(command_window, terminal_y_size - 3, 0);

	// History window
	wresize(history_window, terminal_y_size - 6, terminal_x_size);
	mvwin(history_window, 0, 0);
	scrollok(history_window, TRUE);

	// Status window
	wresize(status_window, 3, terminal_x_size);
	mvwin(status_window, terminal_y_size - 6, 0);
	box(status_window, 0, '#');

	// Refresh so the windows we made actually exist
	refresh();

	// Label history window
	wattron(history_window, A_BOLD);
	wattron(history_window, COLOR_PAIR(4));
	mvwprintw(history_window, 0, 0, "History:\n\n");
	wattroff(history_window, COLOR_PAIRS);
	wattroff(history_window, A_BOLD);

	// Label status window
	wattron(status_window, A_BOLD);
	mvwprintw(status_window, 0, 1, "Status:");
	wattroff(status_window, A_BOLD);

	// Initial message
	wprintw(history_window, "WELCOME TO BABOON SEEKER XTREME CURSES!\n");
	wprintw(history_window, "You are a mighty baboon seeker who has set off into the linguistic plane with %d bamboo to use for seeking baboons\n", bamboo);
	
	// Status window text
	wattron(status_window, A_BOLD);
	wattron(status_window, COLOR_PAIR(1));
	mvwprintw(status_window, 1, 1, "Bamboo:");
	wattron(status_window, COLOR_PAIR(2));
	mvwprintw(status_window, 1, 21, "Baboons:");
	wattron(status_window, COLOR_PAIR(3));
	mvwprintw(status_window, 1, 41, "Backpack:");
	wattroff(status_window, COLOR_PAIRS);
	wattroff(status_window, A_BOLD);


	
	wrefresh(history_window);
	wrefresh(status_window);

	
	while (running){
		// Update status
		mvwprintw(status_window, 1, 9, "%d    ", bamboo);
		mvwprintw(status_window, 1, 30, "%d    ", baboons);
		mvwprintw(status_window, 1, 51, "%d    ", backpack);

		wrefresh(status_window);

		// Display prompt in command window
		char *command_prompt = strdup("Command: ");
		mvwprintw(command_window, 1, 1, "%s", command_prompt);
		// Clear rest of line for command
		wclrtoeol(command_window);
		// Box the command window
		box(command_window, 0, 0);

		// Make sure the user has a backpack
		if (backpack < 1){
			wprintw(history_window, "OH NO! YOU HAVE DROPPED YOUR BABOON SEEKING BACKPACK AND NOW MUST RETIRE FROM SEEKING FOREVER");
			wrefresh(history_window);
			getch();
			break;
		}

		// Read in a command
		bool reading_command = TRUE;
		char command[MAX_COMMAND_LENGTH];
		unsigned char_index = 0;
		while (reading_command){
			char input = wgetch(command_window);
			switch(input){
				case EOF:
					running = FALSE;
					break;
				case '\n':
					command[char_index] = 0;
					reading_command = FALSE;
					break;

				case '\b':
				case 127:
					// Backspace
					// Don't allow backspace past beginning of buffer
					if (char_index <= 0){
						break;
					}

					// Get current cursor position
					int y, x;
					getyx(command_window, y, x);
					// Move cursor back one column
					wmove(command_window, y, x-1);
					// Delete the character
					wdelch(command_window);

					// Remove the character from the command buffer
					char_index--;
					command[char_index] = 0;

					// Refresh the window
					wrefresh(command_window);
					break;
				default:
					// Ignore weird things
					if (input < 20 || input > 126){
						break;
					}

					wprintw(command_window, "%c", input);
					command[char_index] = input;
					char_index++;
					
					// Stop reading the command if we've reached the length limit
					if (char_index >= MAX_COMMAND_LENGTH){
						reading_command = FALSE;
					}
					break;
			}
		}
		
		// Print command to history
		wprintw(history_window, "%s:\n", command);

		// Do game logic
		wprintw(history_window, "%s\n", game_logic(command));



		// Refresh history window
		wrefresh(history_window);

	}


	endwin();
	return 0;
}
