program baboonSeeker;
uses crt, sysutils;

var
	bamboo: integer;
	baboons: integer;
	cmd: string;
	selector: integer;


	function stumble() : Integer;

	begin
		selector := random(100);

		if selector<5 then
		begin
			while true do
			begin
				writeln('FAIL!!!');
			end;
			exit(0);
		end;
		if (selector<40) and (baboons>0) then
		begin
			writeln('You stumbled and dropped a baboon!');
			baboons := baboons - 1;
			exit(0);
		end;
		if bamboo>0 then
		begin
			writeln('You stumbled and dropped a bamboo!');
			bamboo := bamboo - 1;
			exit(0);
		end;

		// No if statements applied, don't drop anything
		writeln('You stumbled!');
	end;

function seek() : Integer;

begin
	// Use 1 bamboo to seek
	bamboo := bamboo - 1;

	// Generate number to select outcome
	selector := random(100);

	// Seek bamboon
	if selector < 1 then
	begin
		writeln('You soke a Bamboon!');
		stumble();
		exit(0);
	end;
	// Seek baboon
	if selector < 10 then
	begin
		if bamboo >= 0 then
		begin
			writeln('You soke a Baboon!');
			baboons := baboons + 1;
		end
		else
		begin
			writeln('You soke a baboon but it ran away because YOU HAS NO BAMBOO!');
		end;
		exit(0);
	end;
	// Seek bamboo
	if selector < 50 then
	begin
		writeln('You soke a Bamboo!');
		// Add 2 since one was used for seeking
		bamboo := bamboo + 2;
		exit(0);
	end;
	// Otherwise, seek nothing
	writeln('You soke a nothing!');

	if bamboo < 0 then
	begin
		bamboo := 0;
	end;
end;

function inventory() : Integer;

begin
	writeln('YOU HAS ' + IntToStr(bamboo) + ' BAMBOO AND YOU HAS ' + IntToStr(baboons) + ' BABOONS AND YOU HAS A BABOON SEEKING BACKPACK');
	exit(0);
end;

begin
	// Seed random number generator
	randomize();

	bamboo := random(15)+5;
	writeln('WELCOME TO BABOON SEEKER XTREME PASCAL');
	writeln('You are a mighty baboon seeker who has set off into the evergreen forest with ' + IntToStr(bamboo) + ' bamboo to use for seeking baboons');

	while true do
		begin
			write(': ');
			readln(cmd);
			case (cmd) of
				'seek': seek();
				'WHAT DOES I HAS': inventory();
				'exit': break;
				else
					stumble();
			end
		end
end.
