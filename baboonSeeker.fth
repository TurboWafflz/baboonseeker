require random.fs
utime drop seed !
\  For some reason the first random number generated is always zero so I'm throwing it away here
10 random drop


\  Initialize things
variable bamboo
15 random
5 +
bamboo !

variable backpack
1 backpack !

variable baboons
0 baboons !

variable commandRecognition
0 commandRecognition !

\  Input
: input
begin
	key
	dup
	dup
	emit
13 = until ;



." WELCOME TO BABOON SEEKER XTREME FORTH" cr

." You are a mighty baboon seeker who has set off into the mangrove with "
bamboo @
.
." bamboo to use for seeking baboons" cr


." : " input

\  Drop enter from end
drop

variable keys
depth cells allot
: command cells keys + ;

: storeCommand
depth 0 do
	i command !
loop ;
storeCommand

0 command ?
1 command ?
2 command ?
3 command ?

: getLetters 0 do
	i command @
loop ;
: checkLetter
	= if
		1 commandRecognition +!
	else
		0 commandRecognition !
then ;

\  Reset number of correct characters to zero
0 commandRecognition !
\  Get characters from array into stack
4 getLetters
\  Check if stack contains seek
CHAR s checkLetter
CHAR e checkLetter
CHAR e checkLetter
CHAR k checkLetter

\  commandRecognition ?
: isSeek
	4 = if
		seek
	then;
isSeek

0 commandRecognition !
15 getLetters
CHAR W checkLetter
CHAR H checkLetter
CHAR A checkLetter
CHAR T checkLetter
BL checkLetter
CHAR D checkLetter
CHAR O checkLetter
CHAR E checkLetter
CHAR S checkLetter
BL checkLetter
CHAR I checkLetter
BL checkLetter
CHAR H checkLetter
CHAR A checkLetter
CHAR S checkLetter

\  commandRecognition ?

