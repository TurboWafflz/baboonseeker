import * as readline from 'readline';

function randInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function fail() {
	let i: number = 1;
	while (i > 0) {
		process.stdout.write('Failure');
		for (let j = 0; j < i; j++) {
			process.stdout.write('!');
		}
		console.log('');
		i++;
	}
}

function stumble(inventory, bamboo) {
	let droppedType: number = randInt(1, 2);
	if (bamboo == 0) {
		droppedType = 1;
	}

	if (droppedType == 1) {
		let droppedItemIndex: number = randInt(0, inventory.length - 1);
		let droppedItem: string = inventory[droppedItemIndex];
		inventory.splice(droppedItemIndex, 1);

		console.log(`You stumbled and dropped a ${droppedItem}`);
		if (droppedItem == 'Baboon Seeking Backpack') {
			fail();
		}
	} else {
		let bamboos: number = randInt(1, bamboo);
		console.log(`You stumbled and dropped ${bamboos} bamboo`);
		bamboo = bamboo - bamboos;
	}
	return [inventory, bamboo];
}

function showInv(inventory, bamboo) {
	process.stdout.write(`YOU HAS ${bamboo} BAMBOO`);
	for (let item of inventory) {
		process.stdout.write(` AND YOU HAS A ${item}`);
	}
	console.log('');
}

function seek(inventory, bamboo) {
	if (bamboo > 0) {
		bamboo--;
	} else {
		console.log('YOU HAS NO BAMBOO!');
	}

	let seeked: number = randInt(0, 100);
	if (seeked < 1) {
		console.log('You soke a Bamboon');
		[inventory, bamboo] = stumble(inventory, bamboo);
	} else if (seeked < 10) {
		if (bamboo > 0) {
			console.log('You soke a Baboon');
			inventory.push('Baboon');
		} else {
			console.log("You soke a Baboon but it ran away because you are out of bamboo");
		}
	} else if (seeked < 50) {
		console.log('You soke a Bamboo');
		bamboo += 2;
	} else {
		console.log('You soke a nothing');
	}
	return [inventory, bamboo];
}

async function* promptUser(query) {
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
	});

	try {
		for (;;) {
			yield new Promise((resolve) => rl.question(query, resolve));
		}
	} finally {
		rl.close();
	}
}

async function main() {
	console.log('WELCOME TO BABOON SEEKER XTREME TYPESCRIPT');
	let bamboo: number = randInt(5, 20);
	let inventory: string[] = ['Baboon Seeking Backpack'];
	console.log(`You are the might baboon seeker. You have set off into the appalachians with your baboon seeking backpack holding ${bamboo} bamboo to use for seeking baboons.`);

	while (true) {
		for await (const cmd of promptUser(": ")) {
			if (cmd == "seek"){
				[inventory, bamboo] = seek(inventory, bamboo);
			}else if(cmd == "WHAT DOES I HAS"){
				showInv(inventory, bamboo);
			}else{
				[inventory, bamboo] = stumble(inventory, bamboo);
			}
		}
	}
}

main();