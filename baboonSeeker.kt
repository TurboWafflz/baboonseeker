@file:Suppress("NAME_SHADOWING")

import java.util.*
import kotlin.Comparator

object BaboonSeeker {
    @JvmStatic
    fun main(args: Array<String>) {
        println("WELCOME TO BABOON SEEKER XTREME KOTLIN")
        var inventory: MutableList<String?> = ArrayList()
        inventory.add("Baboon Seeking Backpack")
        var seekedThing: String? = null
        var command: String
        var bamboo = 5 + (Math.random() * (20 - 5 + 1)).toInt()
        println("You are the mighty baboon seeker. You have set off into the rainforest with your baboon seeking backpack holding $bamboo bamboo to use for seeking baboons.")
        var streak = 0
        while (true) {
            if (!inventory.contains("Baboon Seeking Backpack")) {
                fail()
            }
            print(": ")
            val scanner = Scanner(System.`in`)
            command = try {
                scanner.nextLine()
            } catch (e: NoSuchElementException) {
                ""
            } catch (e: IndexOutOfBoundsException) {
                ""
            }
            if (command == "seek") {
                println("Seeking...")
                val seeked = (Math.random() * (100 + 1)).toInt()
                if (bamboo > 0) {
                    bamboo--
                } else {
                    println("You do not have a bamboo. You must seek one before you will be able to seek baboons.")
                }
                if (seeked == 0) {
                    streak = 0
                    println("You soke a Bamboon")
                    seekedThing = "Bamboon"
                    val stumbled = stumble(inventory, bamboo)
                    inventory = stumbled[0] as MutableList<String?>
                    bamboo = stumbled[1] as Int
                } else if (seeked in 1..10) {
                    if (bamboo > 0) {
                        streak++
                        seekedThing = "Baboon"
                    } else {
                        println("You soke a baboon but it ran away.")
                    }
                } else if (seeked in 12..30) {
                    streak = 0
                    val bamboos = 2 + (Math.random() * (12 - 2 + 1)).toInt()
                    seekedThing = "$bamboos Bamboo"
                    bamboo += bamboos
                } else {
                    streak = 0
                    seekedThing = "nothing"
                }
                println("You soke a $seekedThing!")
                if (streak > 1) {
                    println("STREAK: $streak")
                }
                assert(seekedThing != null)
                if (seekedThing != "nothing" && !seekedThing!!.contains("Bamboo")) {
                    inventory.add(seekedThing)
                }
            } else if (command == "WHAT DOES I HAVE") {
                inventory.sortWith(Comparator.naturalOrder<String>())
                for (item in inventory) {
                    if (item == "Baboon Seeking Backpack") {
                        println("YOU HAS $bamboo BAMBOO IN YOUR BABOON SEEKING BACKPACK")
                    } else {
                        println("YOU HAS A $item")
                    }
                }
            } else if (command == "") {
            } else {
                streak = 0
                val stumbled = stumble(inventory, bamboo)
                inventory = stumbled[0] as MutableList<String?>
                bamboo = stumbled[1] as Int
            }
        }
    }

    private fun fail() {
        var points = 0
        while (true) {
            print("Failure")
            for (i in 0 until points) {
                print("!")
            }
            println()
            points++
        }
    }

    private fun stumble(inventory: List<String?>, bamboo: Int): List<Any> {
        var bamboo = bamboo
        val inventory = inventory.toMutableList()
        val droppedItem = (Math.random() * (inventory.size - 1 + 1)).toInt()
        if (droppedItem < inventory.size) {
            println("You stumbled and dropped a " + inventory[droppedItem])
            //            System.out.println("[Press enter to continue]");
//            Scanner otherScanner = new Scanner(System.in);
//            try {
//                otherScanner.nextLine();
//            }catch(NoSuchElementException e){
//                ;
//            }
//            otherScanner.close();
            inventory.removeAt(droppedItem)
        } else {
            val bamboos = 1 + (Math.random() * (bamboo - 1 + 1)).toInt()
            println("You stumbled and dropped $bamboos bamboo")
            //            System.out.println("[Press enter to continue]");
//            Scanner otherScanner = new Scanner(System.in);
//            try {
//                otherScanner.nextLine();
//            }catch(NoSuchElementException e){
//                ;
//            }
//            otherScanner.close();
            inventory.removeAt(droppedItem)
            bamboo -= bamboos
        }
        return listOf(inventory, bamboo)
    }
}