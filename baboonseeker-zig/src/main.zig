const std = @import("std");
const stdout = std.io.getStdOut().writer();

fn print(text: []const u8) void {
    stdout.print("{s}", .{text}) catch |err| {
        std.debug.print("Error: {}", .{err});
    };
}

fn printInt(number: u32) void {
    stdout.print("{}", .{number}) catch |err| {
        std.debug.print("Error: {}", .{err});
    };
}

fn seek(bamboo: *u32, inventory: *std.ArrayList([]const u8)) void {
    if (bamboo.* > @as(u32, 0)) {
        bamboo.* -= @as(u32, 1);
    } else {
        print("YOU HAS NO BAMBOO!");
    }

    const rand = std.crypto.random;

    switch (rand.intRangeLessThan(u8, 0, 100)) {
        0...1 => {
            print("You soke a Bamboon");
            stumble(bamboo, inventory);
        },
        2...10 => {
            if (bamboo.* > @as(u32, 0)) {
                print("You soke a Baboon");
                inventory.append("Baboon") catch {
                    print("It's all broken");
                };
            } else {
                print("You soke a Baboon but it ran away because YOU HAS NO BAMBOO");
            }
        },
        11...50 => {
            print("You soke a Bamboo");
            bamboo.* += 2;
        },
        else => {
            print("You soke a nothing");
        },
    }
}

fn showInv(bamboo: *u32, inventory: *std.ArrayList([]const u8)) void {
    print("YOU HAS {} BAMBOO");
    printInt(bamboo.*);
    const inventorySlice = inventory.toOwnedSlice() catch |err| {
        std.debug.print("{}", .{err});
        return;
    };
    for (inventorySlice) |item| {
        print(" AND YOU HAS A ");
        print(item);
    }
}

fn stumble(bamboo: *u32, inventory: *std.ArrayList([]const u8)) void {
    print("Stumbling with");
    printInt(bamboo.*);
    _ = inventory;
}

pub fn main() !void {
    print("WELCOME TO BABOON SEEKER XTREME ZIG\n");

    const rand = std.crypto.random;

    // Generate number for bamboo
    var bamboo: u32 = rand.intRangeLessThan(u16, 5, 20);

    var inventory = std.ArrayList([]const u8).init(std.heap.page_allocator);
    _ = try inventory.append("Baboon Seeking Backpack");

    print("You are the mighty baboon seeker. You have set off into the coordinate plane with your baboon seeking backpack holding {} bamboo to use for seeking baboons.\n");

    // Create stdin reader
    const stdin = std.io.getStdIn().reader();

    while (true) {
        print(": ");

        // Buffer to store command
        var cmd: [20]u8 = undefined;
        // Read command from stdin
        _ = try stdin.readUntilDelimiter(&cmd, '\n');
        print(&cmd);

        if (std.mem.eql(u8, &cmd, "seek\n")) {
            seek(&bamboo, &inventory);
        } else if (std.mem.eql(u8, &cmd, "WHAT DOES I HAS\n")) {
            showInv(&bamboo, &inventory);
        } else {
            stumble(&bamboo, &inventory);
        }
    }
}
